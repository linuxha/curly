#!/bin/bash

# Tim needs a bash script that can keep looping and grep for a line
# but it needs to be valid JSON output even if it's more than one
# line

#
help() {
    echo "$(basename $0) [-h] [-c loop_count] [-g egrep_string] [-p password] [-s sleep_time] [-u userid]"
    exit 1
}

# The '!' is problematic in ksh and bash
URL="${URL-http://127.0.0.1/data/sample.json}"
SLEEPVALUE=5

# This part is a work in progress
# The starting colon make getopt silent on reporting non-option args
while getopts ":c:g:p:s:u:h" opt; do
    case $opt in
	c)
	    LOOP=$OPTARG
	    ;;
	g)
	    SOI="$OPTARG"	# String of Interest
	    ;;
	u)
	    user=$OPTARG
	    ;;
	p)
	    pswd=$OPTARG
	    ;;
	s)
	    SLEEPVALUE=$OPTARG
	    ;;
	h)
	    help
	    ;;
	\?) 			# Basically this is the default
	    echo "Unknown option $OPTARG"
	    print_usage
	    exit 3
	    ;;
    esac
done

set -e

JSON='[]'
sleep=0
while [ "${JSON}" == '[]' ]; do
	sleep ${sleep}
	# Yes it's just a newline in the IFS here
	IFS=$'\r\n' JSON=($(curl -s -i "${URL}"))
	ECODE=$?
	if [ ${ECODE} != 0 ]; then
	    >&2 echo "Failed"
	    exit ${ECODE}
	fi

	RESULT=$(expr match "${JSON[0]}" '.* \([0-9][0-9][0-9]*\)')
	# HTTP/1.1 200 OK
	if [ "${RESULT}" != "200" ]; then
	    >&2 echo "${RESULT}"
	    exit 1
	fi
	# ["    <- start, change to [\n"
	# }","{ <- end of a string, change to },\n{
	# "]    <- end of the array change to "\n]
	# Not sure how to get rid of the extra comma on the last string yet
	#RTNSTR=$(echo "${JSON[@]:0}" | \
	RTNSTR=$(echo "${JSON[-1]}" | \
			sed -e "s/\"\]/\n]/g" \
			    -e "s/\[\"/[\n/g" \
			    -e 's/}","{/},\n{/g' \
			    -e 's#\\##g' \
			| egrep "${SOI}")	# this should be the JSON array of stings
	# [ ... ]

	###
	if [ -z "${RTNSTR}" ]; then
	    >&2 echo "Not match"
	    # @FIXME: we could have no match, so that's a [ ]
	    # Basically, I didn't find what I wanted so keep looping
	    # until I do
	    exit 1		# For testing I only have one file
	else
	    COUNT=$(echo "${RTNSTR}" | wc -l)
	    # Now remove the n (${COUNT) instance of "},"
	    RTNSTR=$(echo "${RTNSTR}" | sed -e "${COUNT}s/\}\,\$/\}/")
	    echo -e "[${RTNSTR}]"
	    exit 0
	fi
	sleep=${SLEEPVALUE}
done
>&2 echo -e "${JSON}\n${RESULT}" 
