#!/bin/bash

# Tim needs a bash that can keep looping

help() {
    echo "$(basename $0) [-h] [-c loop_count] [-s sleep_time]"
    exit 1
}

# The '!' is problematic in ksh and bash

URL="${URL-http://127.0.0.1/data/sample.json}"
SLEEPVALUE=5

# This part is a work in progress
# The starting colon make getopt silent on reporting non-option args
while getopts ":u:p:s:h" opt; do
    case $opt in
	u)
	    user=$OPTARG
	    ;;
	p)
	    pswd=$OPTARG
	    ;;
	s)
	    SLEEPVALUE=$OPTARG
	    ;;
	h)
	    help
	    ;;
	\?) 			# Basically this is the default
	    echo "Unknown option $OPTARG"
	    print_usage
	    exit 3
	    ;;
    esac
done


JSON='[]'
sleep=0
while [ "${JSON}" == '[]' ]; do
	sleep ${sleep}
	# Yes it's just a newline in the IFS here
	IFS=$'\r\n' mylines=($(curl -s -i "${URL}"))
	ECODE=$?
	if [ ${ECODE} != 0 ]; then
	    echo "Failed"
	    exit ${ECODE}
	fi

	RESULT=$(expr match "${mylines[0]}" '.* \([0-9][0-9][0-9]*\)')
	# HTTP/1.1 200 OK
	if [ "${RESULT}" != "200" ]; then
	    echo "${RESULT}"
	    exit 1
	fi
	# ["    <- start, change to [\n"
	# }","{ <- end of a string, change to }",\n"{
	# "]    <- end of the array change to "\n]
	# Not sure how to get rid of the extra comma on the last string yet
	#echo "${mylines[@]:0}" | \
	echo "${mylines[-1]}" | \
	    sed -e "s/\"\]/\"\n]/g" -e "s/\[\"/[\n\"/g" -e "s/\}\"\,\"/\}\",\n\"/g"
	    #  this should be the JSON array of stings
	# [ ... ]
	sleep=${SLEEPVALUE}
done
echo -e "${mylines}\n${RESULT}"
