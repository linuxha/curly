# curl tools READEM

A bunch of tools I built, for my own needs, to pull data from a web servive (a URL) and take the expected JSON output and manipulate it.

# Description

## curl.sh

## curl-loop.sh

## curl-loop-grep.sh

## clean_json_a.sh

## clean_json_b.sh

# Example

```
["{\"field1\":\"value\"}","{\"field2\":\"value\"}","{\"field3\":\"value\"}","{\"field4\":\"value\"}"]
```
```
str='["{\"field1\":\"value\"}","{\"field2\":\"value\"}","{\"field3\":\"value\"}","{\"field4\":\"value\"}"]'
echo "${str}" | \
    bash ./clean_json_a.sh | \
    egrep 'field2|field3' | \
    bash clean_json_b.sh | \
    python -m json.tool
```
Output:
```
[
    {
        "field2": "value"
    },
    {
        "field3": "value"
    }
]
```

```
bash ./curl.sh http://127.0.0.1/sample.json | \
    bash ./clean_json_a.sh | \
    egrep 'field2|field3' | \
    bash clean_json_b.sh | \
    python -m json.tool
```

# Author
Neil Cherry <linuxha@linuxha.com

# Copyright
2017

# License
Private, not for public use