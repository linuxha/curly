#!/bin/bash

# Tim needs a bash that can keep looping
help() {
    >&2 echo "$(basename $0) [-d] [-h] [-u username ] [-p password] [-v] [-V \"optons\"] url"

    >&2 cat <<EOF
        -d - debug info to curl.log
	-h - this help screen
	-u - dmaapUserName
	-p - dmapPassword
	-v - verbose
	-V - special option passing

        # JSON in sample.json
        ["{\"field1\":\"value\"}","{\"field2\":\"value\"}","{\"field3\":\"value\"}","{\"field4\":\"value\"}"]

        curl.sh -v "-v -m 25" -u njc -p passwd http://127.0.0.1/sample.json | \\
            bash ./clean_json_a.sh | \\
            egrep 'field2|field3' | \\
            bash clean_json_b.sh | \\
            python -m json.tool

        # Results in
        [
            {
                "field2": "value"
            },
            {
                "field3": "value"
            }
        ]

        # Example of a HTTP 404 response
        curl.sh -v "-v -m 25" -u njc -p passwd http://127.0.0.1/no-sample.json
        2> 404
EOF

    exit 1
}

# getopts
while getopts ":u:p:v:h" opt; do
    case $opt in
	u)
	    user=$OPTARG
	    ;;
	p)
	    pswd=$OPTARG
	    ;;
	v)
	    myOptions=$OPTARG
	    myOptions="-v ${myOptions}"
	    ;;
	d)
	    myDBG=1
	    ;;
	h)
	    help
	    ;;
	\?) 			# Basically this is the default
	    >&2 echo "Unknown option $OPTARG"
	    help
	    exit 3
	    ;;
    esac
done
# get rid of what was handled above and grab the url
shift $(expr $OPTIND - 1 )

#echo "Number of args $#"
if [ $# != 1 ]; then
    >&2 echo "incorrect number of arguments (${#})"
    exit 1
fi

URL="${1}"
if [ -z "${URL}" ]; then
    >&2 echo "missing URL"
    exit 1
fi

# Assemble the user (user:password) option
if [ ! -z "${user}" ]; then
    USERPSWD="${user}"
fi
if [ ! -z "${user}" ]; then
    USERPSWD="${USERPSWD}:${user}"
fi
if [ ! -z "${USERPSWD}" ]; then
    USERPSWD="-u ${USERPSWD}"
fi

# Yes it's an adjusted IFS here
if [ -z "$myOptions}" ]; then
    IFS=$'\r\n' mylines=($(curl -v ${myOptions} -s -i "${USERPSWD}" "${URL}"))
else
    IFS=$'\r\n' mylines=($(curl -s -i "${USERPSWD}" "${URL}"))
fi
ECODE=$?
if [ ! -z "${myDBG}" ]; then
    echo "${mylines[@]}" > curl.log
fi
if [ ${ECODE} != 0 ]; then
    >&2 echo "2> Failed"
    exit ${ECODE}
fi

RESULT=$(expr match "${mylines[0]}" '.* \([0-9][0-9][0-9]*\)')
# HTTP/1.1 200 OK
#echo "${mylines[@]:0}"
# [ ... ]
if [ "${RESULT}" != "200" ]; then
    >&2 echo "2> ${RESULT}"
    exit 1
fi

echo "${mylines[-1]}"
#>2& echo -e "2> ${mylines}\n2> ${RESULT}"
