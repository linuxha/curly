#!/bin/bash

# reads from stdin
RTNSTR=$(cat)
COUNT=$(echo "${RTNSTR}" | wc -l)
# Now remove the n (${COUNT) instance of "},"
RTNSTR=$(echo "${RTNSTR}" | sed -e "${COUNT}s/\}\,\$/\}/")
echo -e "[${RTNSTR}]"
